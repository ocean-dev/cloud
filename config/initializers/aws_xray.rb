if Rails.env.production? && ENV['AWS_CONFIG_LOCAL_MODE'].blank?
  Rails.application.config.xray = {
    name: APP_NAME,
    patch: %I[net_http aws_sdk],
    active_record: true
  }
end
