# DynamoTables -------------------------------------------------------------
$dynamo = Aws::DynamoDB::Client.new(aws_config(endpoint: 'AWS_DYNAMODB_ENDPOINT'))

# Instances ----------------------------------------------------------------
$ec2 = Aws::EC2::Client.new(aws_config)

# Resource oriented interface to EC2 ---------------------------------------
$EC2 = Aws::EC2::Resource.new(client: $ec2)

# CloudWatch client --------------------------------------------------------
$cloudwatch = Aws::CloudWatch::Client.new(aws_config(endpoint: 'AWS_CLOUDWATCH_ENDPOINT'))

# Elastic load balancers ---------------------------------------------------
$elb = Aws::ElasticLoadBalancing::Client.new(aws_config)

# SNS Push notifications ---------------------------------------------------
sns_client = Aws::SNS::Client.new(aws_config(endpoint: 'AWS_SNS_ENDPOINT'))
$sns = Aws::SNS::Resource.new(client: sns_client)
