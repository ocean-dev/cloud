# == Route Map
#
#                        Prefix Verb   URI Pattern                                 Controller#Action
#             refresh_instances PUT    /v1/instances/refresh(.:format)             instances#refresh
#                start_instance PUT    /v1/instances/:id/start(.:format)           instances#start {:id=>/.+/}
#                 stop_instance PUT    /v1/instances/:id/stop(.:format)            instances#stop {:id=>/.+/}
#               reboot_instance PUT    /v1/instances/:id/reboot(.:format)          instances#reboot {:id=>/.+/}
#            terminate_instance DELETE /v1/instances/:id/terminate(.:format)       instances#terminate {:id=>/.+/}
#         decommission_instance PUT    /v1/instances/:id/decommission(.:format)    instances#decommission {:id=>/.+/}
#         recommission_instance PUT    /v1/instances/:id/recommission(.:format)    instances#recommission {:id=>/.+/}
#                     instances GET    /v1/instances(.:format)                     instances#index
#                      instance GET    /v1/instances/:id(.:format)                 instances#show {:id=>/.+/}
#     test_tables_dynamo_tables DELETE /v1/dynamo_tables/test_tables(.:format)     dynamo_tables#delete_test_tables
#                 dynamo_tables GET    /v1/dynamo_tables(.:format)                 dynamo_tables#index
#                  dynamo_table GET    /v1/dynamo_tables/:id(.:format)             dynamo_tables#show {:id=>/[a-zA-Z0-9_.-]{3,255}/}
#                               DELETE /v1/dynamo_tables/:id(.:format)             dynamo_tables#destroy {:id=>/[a-zA-Z0-9_.-]{3,255}/}
#         acknowledge_semaphore PUT    /v1/semaphores/:id/acknowledge(.:format)    semaphores#acknowledge {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#                    semaphores GET    /v1/semaphores(.:format)                    semaphores#index
#                               POST   /v1/semaphores(.:format)                    semaphores#create
#                     semaphore GET    /v1/semaphores/:id(.:format)                semaphores#show {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#                               DELETE /v1/semaphores/:id(.:format)                semaphores#destroy {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#                load_balancers GET    /v1/load_balancers(.:format)                load_balancers#index
#                 load_balancer GET    /v1/load_balancers/:id(.:format)            load_balancers#show {:id=>/[a-zA-Z0-9_.-]{1,255}/}
# supervise_auto_scaling_groups PUT    /v1/auto_scaling_groups/supervise(.:format) auto_scaling_groups#supervise
#           auto_scaling_groups GET    /v1/auto_scaling_groups(.:format)           auto_scaling_groups#index
#                               POST   /v1/auto_scaling_groups(.:format)           auto_scaling_groups#create
#            auto_scaling_group GET    /v1/auto_scaling_groups/:id(.:format)       auto_scaling_groups#show {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#                               PATCH  /v1/auto_scaling_groups/:id(.:format)       auto_scaling_groups#update {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#                               PUT    /v1/auto_scaling_groups/:id(.:format)       auto_scaling_groups#update {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#                               DELETE /v1/auto_scaling_groups/:id(.:format)       auto_scaling_groups#destroy {:id=>/([0-9a-f]+-){4}[0-9a-f]+/}
#                         alive GET    /alive(.:format)                            alive#index
#

Cloud::Application.routes.draw do

  scope "v1" do

    resources :instances, only: [:index, :show],
              constraints: {id: /.+/} do

      collection do
        put    "refresh"
      end

      member do
        put    "start"
        put    "stop"
        put    "reboot"
        delete "terminate"
        put    "decommission"
        put    "recommission"
      end
    end

    resources :dynamo_tables, only: [:index, :show, :destroy],
              constraints: {id: /[a-zA-Z0-9_.-]{3,255}/} do
      collection do
        delete "test_tables", to: "dynamo_tables#delete_test_tables"
      end
    end

    resources :load_balancers, only: [:index, :show],
              constraints: {id: /[a-zA-Z0-9_.-]{1,255}/}

    get    "log_excerpts/:from/:to" => "log_excerpts#show"

  end

end
