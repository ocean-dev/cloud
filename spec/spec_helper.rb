require 'simplecov'
SimpleCov.start do
  add_filter "/vendor/"
  add_filter "/spec/support/hyperlinks.rb"
end

# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV["RAILS_ENV"] ||= 'test'
require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'

# Requires supporting ruby files with custom matchers and macros, etc,
# in spec/support/ and its subdirectories.
Dir[Rails.root.join("spec/support/**/*.rb")].each {|f| require f}

# # DynamoDB table cleaner
# OCEAN_ENV = "master" unless defined?(OCEAN_ENV)
# regexp = Regexp.new("^.+_#{OCEAN_ENV}_[0-9]{1,3}-[0-9]{1,3}-[0-9]{1,3}-[0-9]{1,3}_test$")
# cleaner = lambda { 
#   c = Aws::DynamoDB::Client.new
#   c.list_tables.table_names.each { |t| c.delete_table({table_name: t}) if t =~ regexp }
# }

RSpec.configure do |config|
  # ## Mock Framework
  #
  # If you prefer to use mocha, flexmock or RR, uncomment the appropriate line:
  #
  # config.mock_with :mocha
  # config.mock_with :flexmock
  # config.mock_with :rr

  # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
  #config.fixture_path = "#{::Rails.root}/spec/fixtures"

  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  config.use_transactional_fixtures = true

  # If true, the base class of anonymous controllers will be inferred
  # automatically. This will be the default behavior in future versions of
  # rspec-rails.
  config.infer_base_class_for_anonymous_controllers = false

  # Run specs in random order to surface order dependencies. If you find an
  # order dependency and want to debug it, you can fix the order by providing
  # the seed, which is printed after each run.
  #     --seed 1234
  config.order = "random"
  
  # Make "FactoryBot" superfluous
  config.include FactoryBot::Syntax::Methods

  # RSpec 3 compatibility
  config.infer_spec_type_from_file_location!

  config.before(:suite) do 
    #WebMock.allow_net_connect!
    #cleaner.call
    #WebMock.disable_net_connect!(:allow_localhost => true)
  end

  config.after(:suite) do
    #cleaner.call
    #WebMock.disable_net_connect!
  end
end

def mock_dynamo_table
      d = double :table,
            table_arn: "the-arn",
            attribute_definitions: [
              OpenStruct.new({:attribute_name=>"created_at", :attribute_type=>"N"}), 
              OpenStruct.new({:attribute_name=>"expires_at", :attribute_type=>"N"}), 
              OpenStruct.new({:attribute_name=>"token", :attribute_type=>"S"}), 
              OpenStruct.new({:attribute_name=>"username", :attribute_type=>"S"})
            ],
            key_schema: [
              OpenStruct.new({:attribute_name=>"username", :key_type=>"HASH"}), 
              OpenStruct.new({:attribute_name=>"expires_at", :key_type=>"RANGE"})
            ],
            global_secondary_indexes: [
              OpenStruct.new(
                {:index_name=>"token_created_at_global", 
                 :key_schema=>[
                   {:attribute_name=>"token", :key_type=>"HASH"}, 
                   {:attribute_name=>"created_at", :key_type=>"RANGE"}
                 ], 
                 :projection=>{:projection_type=>"ALL"}, 
                 :index_status=>"ACTIVE", 
                 :provisioned_throughput=>{
                   :last_increase_date_time=>Time.parse("2015-11-17T20:15:17Z"), 
                   :last_decrease_date_time=>Time.parse("2015-11-17T20:16:29Z"), 
                   :number_of_decreases_today=>0, 
                   :read_capacity_units=>20, 
                   :write_capacity_units=>10
                 }, 
                 :index_size_bytes=>1858, 
                 :item_count=>12, 
                 :index_arn=>"arn:aws:dynamodb:eu-west-1:939005974917:table/authentications_master/index/token_created_at_global"
                }
              )
            ],
            local_secondary_indexes: [],
            provisioned_throughput: {
              :last_increase_date_time=>Time.parse("2015-11-17T20:15:17Z"), 
              :last_decrease_date_time=>Time.parse("2015-11-17T20:16:29Z"), 
              :number_of_decreases_today=>0, 
              :read_capacity_units=>20, 
              :write_capacity_units=>10
            },
            table_status: "ACTIVE",
            item_count: 12,
            table_size_bytes: 12345,
            stream_specification: {foo: "bar"},
            latest_stream_arn: "the-stream-arn",
            latest_stream_label: "the-stream-label",
            creation_date_time: Time.parse("2015-04-02")
      allow_any_instance_of(Aws::DynamoDB::Table).
        to receive(:load).
        and_return d
end

