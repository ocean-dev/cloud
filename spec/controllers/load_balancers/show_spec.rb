require 'spec_helper'

describe LoadBalancersController do
  
  render_views

  describe "GET" do
    
    before :each do
      permit_with 200
      allow($elb).to receive(:describe_load_balancers).with(load_balancer_names: ['foo']).
        and_return(double(load_balancer_descriptions: [
                            {'load_balancer_name' => 'foo', 'created_time' => 1.year.ago.utc} 
                          ]))
      allow_any_instance_of(LoadBalancer).to receive(:get_metrics).and_return({})
      request.headers['HTTP_ACCEPT'] = "application/json"
      request.headers['X-API-Token'] = "totally-fake"
    end

    
    it "should return JSON" do
      get :show, params: { id: 'foo' }
      expect(response.content_type).to eq("application/json")
    end
    
    it "should return a 400 if the X-API-Token header is missing" do
      request.headers['X-API-Token'] = nil
      get :show, params: { id: 'foo' }
      expect(response.status).to eq(400)
      expect(response.content_type).to eq("application/json")
    end
    
    it "should return a 404 when the user can't be found" do
      allow($elb).to receive(:describe_load_balancers).with(load_balancer_names: ['nix']).
        and_return(double(load_balancer_descriptions: []))
      get :show, params: { id: "nix" }
      expect(response.status).to eq(404)
      expect(response.content_type).to eq("application/json")
    end
    
    it "should return a 200 when successful" do
      get :show, params: { id: 'foo' }
      expect(response.status).to eq(200)
      expect(response).to render_template(partial: "_load_balancer", count: 1)
    end
    
  end
  
end
