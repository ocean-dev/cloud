require 'spec_helper'

describe DynamoTablesController do
  
  render_views

  describe "GET" do
    
    before :each do
      permit_with 200
      mock_dynamo_table
      @dynamo_table = build :dynamo_table
      @dynamo_table.valid?
      request.headers['HTTP_ACCEPT'] = "application/json"
      request.headers['X-API-Token'] = "totally-fake"
    end

    
    it "should return JSON" do
      get :show, params: { id: @dynamo_table.name }
      expect(response.content_type).to eq("application/json")
    end
    
    it "should return a 400 if the X-API-Token header is missing" do
      request.headers['X-API-Token'] = nil
      get :show, params: { id: @dynamo_table.name }
      expect(response.status).to eq(400)
      expect(response.content_type).to eq("application/json")
    end
    
    it "should return a 404 when the table can't be found" do
      expect_any_instance_of(Aws::DynamoDB::Table).to receive(:load).and_raise
      get :show, params: { id: "NOWAI" }
      expect(response.status).to eq(404)
      expect(response.content_type).to eq("application/json")
    end
    
    it "should return a 200 when successful" do
      get :show, params: { id: @dynamo_table.name }
      expect(response.status).to eq(200)
      expect(response).to render_template(partial: "_dynamo_table", count: 1)
    end
    
  end
  
end
