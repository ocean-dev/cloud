require 'spec_helper'

describe DynamoTablesController do

  render_views

  describe "DELETE" do

    before :each do
      permit_with 200
      mock_dynamo_table
      @t = build :dynamo_table
      @t.valid?
      request.headers['HTTP_ACCEPT'] = "application/json"
      request.headers['X-API-Token'] = "so-totally-fake"
    end


    it "should return a 400 if the X-API-Token header is missing" do
      expect(@t.table).not_to receive(:delete)
      request.headers['X-API-Token'] = nil
      delete :destroy, params: { id: @t.name }
      expect(response.status).to eq(400)
    end

    it "should return a 204 when successful" do
      expect(@t.table).to receive(:delete)
      delete :destroy, params: { id: @t.name }
      expect(response.status).to eq(204)
      expect(response.content_type).to eq nil
    end

    it "should return a 404 when the Semaphore can't be found" do
      expect_any_instance_of(Aws::DynamoDB::Table).to receive(:load).and_raise
      expect(@t.table).not_to receive(:delete)
      delete :destroy, params: { id: "NOWAI" }
      expect(response.status).to eq(404)
    end

  end

end
