require 'spec_helper'
require 'ostruct'

describe DynamoTable do

  describe "attributes" do

    it "should include name" do
      expect(build :dynamo_table).to respond_to :name
    end

    it "should require a name" do
      expect(build :dynamo_table, name: "").to_not be_valid
    end

    it "should include table_arn" do
      expect(build :dynamo_table).to respond_to :table_arn
    end

    it "should include attribute_definitions" do
      expect(build :dynamo_table).to respond_to :attribute_definitions
    end

    it "should include key_schema" do
      expect(build :dynamo_table).to respond_to :key_schema
    end

    it "should include global_secondary_indexes" do
      expect(build :dynamo_table).to respond_to :global_secondary_indexes
    end

    it "should include local_secondary_indexes" do
      expect(build :dynamo_table).to respond_to :local_secondary_indexes
    end

    it "should include provisioned_throughput" do
      expect(build :dynamo_table).to respond_to :provisioned_throughput
    end

    it "should include table_status" do
      expect(build :dynamo_table).to respond_to :table_status
    end

    it "should include item_count" do
      expect(build :dynamo_table).to respond_to :item_count
    end

    it "should include table_size_bytes" do
      expect(build :dynamo_table).to respond_to :table_size_bytes
    end

    it "should include stream_specification" do
      expect(build :dynamo_table).to respond_to :stream_specification
    end

    it "should include latest_stream_arn" do
      expect(build :dynamo_table).to respond_to :latest_stream_arn
    end

    it "should include latest_stream_label" do
      expect(build :dynamo_table).to respond_to :latest_stream_label
    end

    it "should include created_at" do
      expect(build :dynamo_table).to respond_to :created_at
    end
  end


  it "should only load the data if not already loaded" do
    expect_any_instance_of(Aws::DynamoDB::Table).to receive(:data_loaded?).and_return true
    expect(build :dynamo_table).to be_valid
  end

  it "should be invalid if the table with the given name doesn't exist" do
    expect_any_instance_of(Aws::DynamoDB::Table).to receive(:load).and_raise
    expect(build :dynamo_table).to_not be_valid
  end

  it "should set the table attribute when valid" do
    expect_any_instance_of(Aws::DynamoDB::Table).to receive(:load).and_return "the-data"
    t = build :dynamo_table
    expect(t.valid?).to be true
    expect(t.table).to eq "the-data"
  end

  it "should set the table attribute to nil when invalid" do
    expect_any_instance_of(Aws::DynamoDB::Table).to receive(:load).and_raise
    t = build :dynamo_table
    expect(t.valid?).to be false
    expect(t.table).to eq nil
  end


  describe "attribute reader" do

    before :each do
      mock_dynamo_table
      @dt = build :dynamo_table
      expect(@dt).to be_valid
    end

    it "table_arn should return the string as is" do
      expect(@dt.table_arn).to eq @dt.table.table_arn
    end

    it "attribute_definitions should convert an array of structs" do
      expect(@dt.attribute_definitions).to eq [
        {:attribute_name=>"created_at", :attribute_type=>"N"},
        {:attribute_name=>"expires_at", :attribute_type=>"N"},
        {:attribute_name=>"token", :attribute_type=>"S"},
        {:attribute_name=>"username", :attribute_type=>"S"}
      ]
    end

    it "key_schema should convert an array of structs" do
      expect(@dt.key_schema).to eq [
        {:attribute_name=>"username", :key_type=>"HASH"},
        {:attribute_name=>"expires_at", :key_type=>"RANGE"}
      ]
    end

    it "global_secondary_indexes should convert an array of structs with included dates" do
      expect(@dt.global_secondary_indexes).to eq [
        {:index_name=>"token_created_at_global",
         :key_schema=>[
           {:attribute_name=>"token", :key_type=>"HASH"},
           {:attribute_name=>"created_at", :key_type=>"RANGE"}
         ],
         :projection=>{:projection_type=>"ALL"},
         :index_status=>"ACTIVE",
         :provisioned_throughput=>{
           :last_increase_date_time=>"2015-11-17T20:15:17Z",
           :last_decrease_date_time=>"2015-11-17T20:16:29Z",
           :number_of_decreases_today=>0,
           :read_capacity_units=>20,
           :write_capacity_units=>10
         },
         :index_size_bytes=>1858,
         :item_count=>12,
         :index_arn=>"arn:aws:dynamodb:eu-west-1:939005974917:table/authentications_master/index/token_created_at_global"
        }
      ]
    end

    it "local_secondary_indexes should convert an array of structs" do
      expect(@dt.local_secondary_indexes).to eq []
    end

    it "provisioned_throughput should convert a hash with included dates" do
      expect(@dt.provisioned_throughput).to eq({
        :last_increase_date_time=>"2015-11-17T20:15:17Z",
        :last_decrease_date_time=>"2015-11-17T20:16:29Z",
        :number_of_decreases_today=>0,
        :read_capacity_units=>20,
        :write_capacity_units=>10
      })
    end

    it "table_status should return the string as is" do
      expect(@dt.table_status).to eq @dt.table.table_status
    end

    it "item_count should return the integer as is" do
      expect(@dt.item_count).to eq @dt.table.item_count
    end

    it "table_size_bytes should return the integer as is" do
      expect(@dt.table_size_bytes).to eq @dt.table.table_size_bytes
    end

    it "stream_specification should convert a hash" do
      expect(@dt.stream_specification).to eq({:foo=>"bar"})
    end

    it "latest_stream_arn should return the string as is" do
      expect(@dt.latest_stream_arn).to eq @dt.table.latest_stream_arn
    end

    it "latest_stream_label should return the string as is" do
      expect(@dt.latest_stream_label).to eq @dt.table.latest_stream_label
    end

    it "created_at should return the Time as is" do
      expect(@dt.created_at).to eq @dt.table.creation_date_time
      expect(@dt.created_at).to be_a Time
    end
  end



  describe ".delete_test_tables" do

  	before :each do
  	  expect(DynamoTable).to receive(:obtain_all_dynamodb_tables).
  	    and_return(["foo_master",
  	    	          "foo_staging",
  	    	          "foo_prod",
                    "foo_test",
  	    	          "bar_mitzvah",
  	    	          "bar_none"
  	    	       ])
  	end


  	it "should obtain the list of DynamoDB table names" do
  	  allow(DynamoTable).to receive(:delete_table)
  	  DynamoTable.delete_test_tables
  	end

  	it "should call purgeable? for each table" do
  	  expect(DynamoTable).to receive(:purgeable?).exactly(6).times.and_return(false)
  	  DynamoTable.delete_test_tables
  	end

  	it "should call delete_table whenever purgeable? is true" do
  	  expect(DynamoTable).to receive(:purgeable?).exactly(6).times.
  	    and_return(true, false, false, true, false, false)
  	  expect(DynamoTable).to receive(:delete_table).exactly(2).times
  	  DynamoTable.delete_test_tables
  	end

  	it "should call delete_table twice for the test table list" do
  	  expect(DynamoTable).to receive(:delete_table).twice
  	  DynamoTable.delete_test_tables
  	end
  end


  describe ".purgeable?" do

    it "should return true if the table name ends in '_test'" do
      expect(DynamoTable.purgeable? "foo_test").to eq true
    end

    it "should return true if the table name ends in '_master'" do
      expect(DynamoTable.purgeable? "foo_master").to eq true
    end

    it "should return false if the table name ends in '_dev'" do
      expect(DynamoTable.purgeable? "foo_dev").to eq false
    end

    it "should return false if the table name ends in '_prod'" do
      expect(DynamoTable.purgeable? "foo_prod").to eq false
    end

  end


  describe ".delete_table" do

  	it "should call AWS properly" do
  	  expect($dynamo).to receive(:delete_table).with({table_name: "godzilla"})
  	  DynamoTable.delete_table("godzilla")
  	end

    it "should wait and retry if an Aws::DynamoDB::Errors::LimitExceededException is thrown" do
      expect($dynamo).to receive(:delete_table).with({table_name: "godzilla"}).
        and_raise(Aws::DynamoDB::Errors::LimitExceededException.new("foo", "bar"))
      expect($dynamo).to receive(:delete_table).with({table_name: "godzilla"})
      expect(Rails.logger).to receive(:debug).with("AWS deletion failed due to deletion limit reached, waiting and retrying...")
      expect(Object).to receive(:sleep).with(10)
      DynamoTable.delete_table("godzilla")
    end

  end

end
