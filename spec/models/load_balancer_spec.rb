require 'spec_helper'

describe LoadBalancer do


  describe "attributes" do
    
    it "should have a load_balancer_name" do
      expect(build(:load_balancer).load_balancer_name).to be_a String
    end

    it "should have a created_time" do
      expect(build(:load_balancer).created_time).to be_a Time
    end

    it "should have a created_at alias to created_time" do
      t = 1.year.ago.utc
      expect(build(:load_balancer, created_time: t).created_at).to eq t
    end

    it "should not have an update time" do
      expect(build(:load_balancer)).not_to respond_to :updated_at
    end
  
    it "should not have a creator" do
      expect(build(:load_balancer)).not_to respond_to :created_by
    end

    it "should not have an updater" do
      expect(build(:load_balancer)).not_to respond_to :updated_by
    end

    it "should not have a lock_version" do
      expect(build(:load_balancer)).not_to respond_to :lock_version
    end

    it "should respond to :availability_zones" do
      expect(build :load_balancer).to respond_to :availability_zones
    end

    it "should respond to :backend_server_descriptions" do
      expect(build :load_balancer).to respond_to :backend_server_descriptions
    end

    it "should respond to :canonical_hosted_zone_name" do
      expect(build :load_balancer).to respond_to :canonical_hosted_zone_name
    end

    it "should respond to :canonical_hosted_zone_name_id" do
      expect(build :load_balancer).to respond_to :canonical_hosted_zone_name_id
    end

    it "should respond to :dns_name" do
      expect(build :load_balancer).to respond_to :dns_name
    end

    it "should respond to :health_check" do
      expect(build :load_balancer).to respond_to :health_check
    end

    it "should respond to :instances" do
      expect(build :load_balancer).to respond_to :instances
    end

    it "should respond to :listener_descriptions" do
      expect(build :load_balancer).to respond_to :listener_descriptions
    end

    it "should respond to :policies" do
      expect(build :load_balancer).to respond_to :policies
    end

    it "should respond to :scheme" do
      expect(build :load_balancer).to respond_to :scheme
    end

    it "should respond to :security_groups" do
      expect(build :load_balancer).to respond_to :security_groups
    end

    it "should respond to :source_security_group" do
      expect(build :load_balancer).to respond_to :source_security_group
    end

    it "should respond to :subnets" do
      expect(build :load_balancer).to respond_to :subnets
    end

    it "should respond to :vpc_id" do
      expect(build :load_balancer).to respond_to :vpc_id
    end

    it "should respond to :metrics" do
      expect(build :load_balancer).to respond_to :metrics
    end
  end


  describe ".all" do

    before :each do
      allow_any_instance_of(LoadBalancer).to receive(:get_metrics).and_return({})
    end

    it "should return an array" do
      expect($elb).to receive(:describe_load_balancers).
        and_return(double(load_balancer_descriptions: [{'load_balancer_name' => 'foo'}, 
                                                       {'load_balancer_name' => 'bar'}, 
                                                       {'load_balancer_name' => 'baz'}],
                          next_marker: nil))
      expect(LoadBalancer.all).to be_an Array
    end

    it "should return all load balancers" do
      expect($elb).to receive(:describe_load_balancers).
        and_return(double(load_balancer_descriptions: [{'load_balancer_name' => 'foo'}, 
                                                       {'load_balancer_name' => 'bar'}, 
                                                       {'load_balancer_name' => 'baz'}],
                          next_marker: nil))
      lbs = LoadBalancer.all
      expect(lbs.length).to eq 3
      expect(lbs.first).to be_a LoadBalancer
      expect(lbs.first.metrics).to eq({})
    end

    it "should transparently handle paging" do
      expect($elb).to receive(:describe_load_balancers).
        with(marker: nil).
        and_return(double(load_balancer_descriptions: [{'load_balancer_name' => 'foo'}, 
                                                       {'load_balancer_name' => 'bar'}, 
                                                       {'load_balancer_name' => 'baz'}],
                          next_marker: "1234567890"))
      expect($elb).to receive(:describe_load_balancers).
        with(marker: "1234567890").
        and_return(double(load_balancer_descriptions: [{'load_balancer_name' => 'blah'}, 
                                                       {'load_balancer_name' => 'quux'}],
                          next_marker: nil))
      lbs = LoadBalancer.all
      expect(lbs.length).to eq 5
    end

  end


  describe ".find_by_load_balancer_name" do

    it "should return only one load balancer matching the name" do
      expect($elb).to receive(:describe_load_balancers).
        with(load_balancer_names: ["bar"]).
        and_return(double(load_balancer_descriptions: [{load_balancer_name: 'bar'}]))
      expect_any_instance_of(LoadBalancer).to receive(:get_metrics).and_return({})
      lb = LoadBalancer.find_by_load_balancer_name('bar')
      expect(lb).to be_a LoadBalancer
      expect(lb.metrics).to eq({})
    end

    it "should return nil if no match is found" do
      expect($elb).to receive(:describe_load_balancers).
        with(load_balancer_names: ["YOKO"]).
        and_return(double(load_balancer_descriptions: []))
      expect_any_instance_of(LoadBalancer).not_to receive(:get_metrics)
      expect(LoadBalancer.find_by_load_balancer_name('YOKO')).to eq nil
    end
  end


  describe "#metrics" do

    it "should call #get_metrics when load_balancer_name is present" do
      expect_any_instance_of(LoadBalancer).to receive(:get_metrics).with(no_args)
      LoadBalancer.new load_balancer_name: 'foo'
    end

    it "should not call #get_metrics when load_balancer_name is blank" do
      expect_any_instance_of(LoadBalancer).not_to receive(:get_metrics)
      LoadBalancer.new load_balancer_name: '      '
    end
  end


  describe "#get_metrics" do

    METRIC_NAMES = %w(HealthyHostCount UnHealthyHostCount RequestCount Latency 
       SurgeQueueLength SpilloverCount HTTPCode_ELB_4XX HTTPCode_ELB_5XX 
       HTTPCode_Backend_2XX HTTPCode_Backend_3XX HTTPCode_Backend_4XX HTTPCode_Backend_5XX
       BackendConnectionErrors
    )

    it "should query AWS for each metric" do
      METRIC_NAMES.each do |metric_name|
        expect($cloudwatch).to receive(:get_metric_statistics).
          with(hash_including(:namespace=>"AWS/ELB", :metric_name=>metric_name, 
                :statistics=>["Sum", "Maximum", "Minimum", "Average"], 
                :dimensions=>[{:name=>"LoadBalancerName", :value=>"quux"}], 
                :period=>60)).
          and_return(double(label: metric_name, datapoints: []))
      end
      lb = LoadBalancer.new load_balancer_name: "quux"
      METRIC_NAMES.each do |metric_name|
        expect(lb.metrics[metric_name]).to eq []
      end
    end

  end

end
