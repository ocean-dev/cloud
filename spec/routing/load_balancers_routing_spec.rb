require "spec_helper"

describe LoadBalancersController do
  describe "routing" do

    it "routes to #index" do
      expect(get("/v1/load_balancers")).to route_to("load_balancers#index")
    end

    it "routes to #show" do
      expect(get("/v1/load_balancers/master-api")).to route_to("load_balancers#show", id: "master-api")
    end

    it "should not route to #create" do
      expect(post("/v1/load_balancers")).not_to be_routable
    end

    it "should not route to #update" do
      expect(put("/v1/load_balancers/1")).not_to be_routable
    end

    it "should not route to #destroy" do
      expect(delete("/v1/load_balancers/1")).not_to be_routable
    end
  end
end
