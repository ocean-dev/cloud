require 'spec_helper'

describe "load_balancers/_load_balancer" do
  
  describe do
    before :each do                     # Must be :each (:all causes all tests to fail)
      render partial: "load_balancers/load_balancer", 
        locals: {load_balancer: build(:load_balancer)}
      @json = JSON.parse(rendered)
      @u = @json['load_balancer']
      @links = @u['_links'] rescue {}
    end


    it "should have one hyperlinks" do
      expect(@links.size).to eq(1)
    end

    it "should have a self hyperlink" do
      expect(@links).to be_hyperlinked('self', /load_balancers/)
    end

    it "has a named root" do
      expect(@u).not_to eq(nil)
    end

    it "should not have an updated_at time" do
      expect(@u.key?('updated_at')).to eq false
    end

    it "should not have a lock_version field" do
      expect(@u.key?('lock_version')).to eq false
    end

    [:load_balancer_name, :created_at, :availability_zones, :backend_server_descriptions,
     :canonical_hosted_zone_name, :canonical_hosted_zone_name_id, :dns_name,
     :source_security_group, :listener_descriptions, :subnets, :instances,
     :vpc_id, :scheme, :policies, :health_check, :security_groups, :metrics
    ].each do |attr|
      it "should have #{attr} present in the JSON representation" do
        expect(@u.key?(attr.to_s)).to be true
      end
    end
        
  end
                  
end
