require 'spec_helper'

describe "dynamo_tables/_dynamo_table" do
  
  describe do
    before :each do                     # Must be :each (:all causes all tests to fail)
      mock_dynamo_table
      t = build(:dynamo_table)
      t.valid?
      render partial: "dynamo_tables/dynamo_table", 
        locals: {dynamo_table: t}
      @json = JSON.parse(rendered)
      @u = @json['dynamo_table']
      @links = @u['_links'] rescue {}
    end


    it "should have one hyperlink" do
      expect(@links.size).to eq(1)
    end

    it "should have a self hyperlink" do
      expect(@links).to be_hyperlinked('self', /dynamo_tables/)
    end

    it "has a named root" do
      expect(@u).not_to eq(nil)
    end

    it "should not have an updated_at time" do
      expect(@u.key?('updated_at')).to eq false
    end

    it "should not have a lock_version field" do
      expect(@u.key?('lock_version')).to eq false
    end

    [:name, :table_arn, :attribute_definitions, :key_schema,
     :global_secondary_indexes, :local_secondary_indexes, :provisioned_throughput,
     :table_status, :item_count, :table_size_bytes, :stream_specification,
     :latest_stream_arn, :latest_stream_label, :created_at
    ].each do |attr|
      it "should have #{attr} present in the JSON representation" do
        expect(@u.key?(attr.to_s)).to be true
      end
    end
        
  end
                  
end
