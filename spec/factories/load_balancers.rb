FactoryBot.define do
  factory :load_balancer do
    load_balancer_name { "master-internal" }
    created_time { 1.month.ago.utc }
  end

end
