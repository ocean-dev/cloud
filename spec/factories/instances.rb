# == Schema Information
#
# Table name: instances
#
#  id                      :string           primary key
#  instance_id             :string
#  name                    :string
#  description             :string
#  ocean_env                :string
#  service                 :string
#  subservice              :string           not null
#  contents                :text
#  created_at              :datetime
#  updated_at              :datetime
#  lock_version            :integer          default(0), not null
#  state                   :string
#  instance_type           :string
#  launch_time             :datetime
#  availability_zone       :string
#  subnet_id               :string
#  private_ip_address      :string
#  state_transition_reason :string
#  vpc_id                  :string
#  public_ip_address       :string
#  security_groups         :text
#  state_reason_message    :string
#  metrics                 :text
#
# Indexes
#
#  index_instances_on_ocean_env_and_name  (ocean_env,name)
#  index_instances_on_id                 (id) UNIQUE
#  index_instances_on_instance_id        (instance_id) UNIQUE
#  instances_main                        (ocean_env,service,subservice,state)
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryBot.define do
  factory :instance do
    instance_id { "i-#{Random.rand(99999999)}" }
    name { "MyString" }
    description { "MyString" }
    ocean_env { "master" }
    service { "auth" }
    subservice { "something" }
    contents { nil }
    state { "running" }
    instance_type { "m1.medium" }
    launch_time { 1.month.ago.utc }
    availability_zone { "eu-west-1a" }
    subnet_id { "7364gfdkf" }
    private_ip_address { "10.10.10.10" }
    public_ip_address { "123.123.123.123" }
    state_transition_reason { "Yllo yllo" }
    vpc_id { "u3ygfsdhfuy" }
    state_reason_message { "sdufhsdf" }
    security_groups { ["siduhfiushf", "sdbfidsuhf", "eruhgh"] }
  end
end
