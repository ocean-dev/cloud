require 'spec_helper'

describe Instance, :type => :request do

  before :each do
    permit_with 200
    Instance.delete_all
    create :instance, instance_id: "i-1",
                              service: "the-service", subservice: ""
    create :instance, instance_id: "i-2",
                              service: "the-service", subservice: "sub1"
    create :instance, instance_id: "i-3",
                              service: "the-service", subservice: ""
    create :instance, instance_id: "i-4",
                              service: "the-service", subservice: "sub2"
    create :instance, instance_id: "i-5",
                              service: "the-service", subservice: "sub2"
    create :instance, instance_id: "j-1",
                              service: "some-other-service", subservice: "sub1"
    @headers = {'HTTP_ACCEPT' => "application/json", 'X-API-TOKEN' => "incredibly-fake"}
  end


  describe do

    it "should return JSON" do
      get "/v1/instances", params: {}, headers: @headers
      expect(response.content_type).to eq "application/json"
    end

    it "should return 200 when successful" do
      get "/v1/instances", params: {}, headers: @headers
      expect(response.status).to eq 200
    end

    it "search with a given subservice should match those subservices" do
      get "/v1/instances", params: {'service' => 'the-service', 'subservice' => "sub1"}, headers: @headers
      b = JSON.parse(response.body)
      coll = b['_collection']
      names = coll['resources'].collect { |r| r['instance']['instance_id'] }
      expect(names).to eq ["i-2"]
    end

    it "search with a blank subservice should use the empty string" do
      get "/v1/instances", params: {'service' => 'the-service', 'subservice' => nil}, headers: @headers
      b = JSON.parse(response.body)
      coll = b['_collection']
      names = coll['resources'].collect { |r| r['instance']['instance_id'] }
      expect(names.length).to eq 2
      expect(names).to include "i-1"
      expect(names).to include "i-3"
    end

    it "search with an empty string (highly unlikely) should use the empty string" do
      get "/v1/instances", params: {'service' => 'the-service', 'subservice' => ''}, headers: @headers
      b = JSON.parse(response.body)
      coll = b['_collection']
      names = coll['resources'].collect { |r| r['instance']['instance_id'] }
      expect(names.length).to eq 2
      expect(names).to include "i-1"
      expect(names).to include "i-3"
    end
  end

end
