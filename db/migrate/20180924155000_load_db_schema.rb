class LoadDbSchema < ActiveRecord::Migration[4.2]
  def up
    create_table "instances", id: false, force: :cascade do |t|
      t.string   "id",                      limit: 255,               null: false
      t.string   "instance_id",             limit: 255
      t.string   "name",                    limit: 255
      t.string   "description",             limit: 255
      t.string   "ocean_env",               limit: 255
      t.string   "service",                 limit: 255
      t.string   "subservice",              limit: 255,               null: false
      t.text     "contents",                limit: 65535
      t.datetime "created_at"
      t.datetime "updated_at"
      t.integer  "lock_version",            limit: 4,     default: 0, null: false
      t.string   "state",                   limit: 255
      t.string   "instance_type",           limit: 255
      t.datetime "launch_time"
      t.string   "availability_zone",       limit: 255
      t.string   "subnet_id",               limit: 255
      t.string   "private_ip_address",      limit: 255
      t.string   "state_transition_reason", limit: 255
      t.string   "vpc_id",                  limit: 255
      t.string   "public_ip_address",       limit: 255
      t.text     "security_groups",         limit: 65535
      t.string   "state_reason_message",    limit: 255
      t.text     "metrics",                 limit: 65535
    end

    add_index "instances", ["id"], name: "index_instances_on_id", unique: true, using: :btree
    add_index "instances", ["instance_id"], name: "index_instances_on_instance_id", unique: true, using: :btree
    add_index "instances", ["ocean_env", "name"], name: "index_instances_on_ocean_env_and_name", using: :btree
    add_index "instances", ["ocean_env", "service", "subservice", "state"], name: "instances_main", using: :btree
  end

  def down
    drop_table "instances"
  end

end
