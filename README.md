# The Cloud Service

This is the Cloud Service for Ocean. It interfaces to AWS and provides means of
controlling and monitoring infrastructure.

## Installation

First start the common infrastructure, if it isn't already running:
```
  docker-compose -f ../ocean/common.yml up -d --build
```
Then, to run only the Cloud service:
```
  docker-compose -f ../ocean/ocean.yml up --build cloud_service
```


## Running the RSpecs

If you intend to run RSpec tests, start the common infrastructure, if it isn't
already running:
```
  docker-compose -f ../ocean/common.yml up --build
```
Next create and migrate the test database:
```
  docker-compose -f ../ocean/ocean.yml run --rm -e RAILS_ENV=test cloud_service rake db:create
  docker-compose -f ../ocean/ocean.yml run --rm -e RAILS_ENV=test cloud_service rake db:migrate
```
The source is accessible through a Docker volume, so any changes you make to the
source will be propagated to the container, but if you modify things like
gems, you need to rebuild the Docker image:
```
  docker-compose -f ../ocean/ocean.yml build cloud_service
```
Then, to run the actual specs:
```
  docker-compose -f ../ocean/ocean.yml run --rm -e LOG_LEVEL=fatal cloud_service rspec
```
