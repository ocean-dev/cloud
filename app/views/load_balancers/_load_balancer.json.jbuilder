json.load_balancer do |json|
	json._links       hyperlinks(self: load_balancer_url(load_balancer.load_balancer_name))
	json.(load_balancer, 
	        :load_balancer_name, 
            :dns_name,
            :canonical_hosted_zone_name, 
            :canonical_hosted_zone_name_id, 
	        :availability_zones, 
	        :backend_server_descriptions,
            :source_security_group, 
            :listener_descriptions, 
            :subnets, 
            :instances,
            :vpc_id, 
            :scheme, 
            :policies, 
            :health_check, 
            :security_groups, 
            :metrics) 
	json.created_at load_balancer.created_at.utc.iso8601
end
