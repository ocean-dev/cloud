json.dynamo_table do |json|
	json._links       hyperlinks(self:    dynamo_table_url(id: dynamo_table.name))
	json.(dynamo_table, :name, 
	                    :table_arn, 
	                    :attribute_definitions, 
	                    :key_schema,
     					:global_secondary_indexes, 
     					:local_secondary_indexes, 
     					:provisioned_throughput,
     					:table_status, 
     					:item_count, 
     					:table_size_bytes, 
     					:stream_specification,
     					:latest_stream_arn, 
     					:latest_stream_label
    ) 
	json.created_at dynamo_table.created_at.utc.iso8601
end
