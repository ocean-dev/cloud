json.instance do |json|
	json.(instance,   :instance_id, 
                    :name, :description, :ocean_env, :service, :subservice,
                    :state, :instance_type, :availability_zone, :subnet_id,
                    :private_ip_address, :public_ip_address, :state_transition_reason,
                    :vpc_id, :state_reason_message, :security_groups,
                    :metrics)
  json.launch_time  instance.launch_time.utc.iso8601 if instance.launch_time
	json.created_at   instance.created_at.utc.iso8601
	json.updated_at   instance.updated_at.utc.iso8601
  json.lock_version instance.lock_version
  links = {}
  links[:self] = instance_url(id: instance.instance_id)
  if instance.state == "stopped"
    links[:self] = instance_url(id: instance.instance_id)
    links[:start] = start_instance_url(id: instance.instance_id)
    links[:terminate] = terminate_instance_url(id: instance.instance_id)
  elsif instance.state == "running"
    links[:stop] = stop_instance_url(id: instance.instance_id)
    links[:reboot] = reboot_instance_url(id: instance.instance_id)
  end
  json._links hyperlinks(links)
end
