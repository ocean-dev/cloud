class DynamoTablesController < ApplicationController

  ocean_resource_controller extra_actions: { 'delete_test_tables' => ['test_tables', "DELETE"]
                                           }
  before_action :find_dynamo_table, except: [:index, :delete_test_tables]


  # GET /dynamo_tables
  def index
    api_render DynamoTable.all
  end


  # GET /dynamo_tables/the-table-name
  def show
    expires_in 0, 's-maxage' => DEFAULT_CACHE_TIME
    if stale?(@dynamo_table)
      api_render @dynamo_table
    end
  end


  # DELETE /dynamo_tables/the-table-name
  def destroy
    @dynamo_table.destroy
    render_head_204
  end


  # DELETE /dynamo_tables/test_tables
  def delete_test_tables
    DynamoTable.delete_test_tables(OCEAN_ENV)
    render_head_204
  end


  private

  def find_dynamo_table
    @dynamo_table = DynamoTable.new name: params[:id]
    return true if @dynamo_table.valid?   # Validations triggers AWS table fetch
    render_api_error 404, "DynamoTable not found"
    false
  end

end
