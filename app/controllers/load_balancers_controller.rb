class LoadBalancersController < ApplicationController
                                           

  # GET /instances
  def index
    @load_balancers = LoadBalancer.all    
    api_render @load_balancers
  end


  # GET /instances/1
  def show
    @load_balancer = LoadBalancer.find_by_load_balancer_name(params[:id])
    render_api_error 404, "Instance not found" and return unless @load_balancer
    api_render @load_balancer
  end
      
end
