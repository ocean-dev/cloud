# == Schema Information
#
# Table name: instances
#
#  id                      :string           primary key
#  instance_id             :string
#  name                    :string
#  description             :string
#  ocean_env                :string
#  service                 :string
#  subservice              :string           not null
#  contents                :text
#  created_at              :datetime
#  updated_at              :datetime
#  lock_version            :integer          default(0), not null
#  state                   :string
#  instance_type           :string
#  launch_time             :datetime
#  availability_zone       :string
#  subnet_id               :string
#  private_ip_address      :string
#  state_transition_reason :string
#  vpc_id                  :string
#  public_ip_address       :string
#  security_groups         :text
#  state_reason_message    :string
#  metrics                 :text
#
# Indexes
#
#  index_instances_on_ocean_env_and_name  (ocean_env,name)
#  index_instances_on_id                 (id) UNIQUE
#  index_instances_on_instance_id        (instance_id) UNIQUE
#  instances_main                        (ocean_env,service,subservice,state)
#

class Instance < ActiveRecord::Base

  self.primary_key = "id"

  # Callbacks
  def initialize(*)
    super
    self.id ||= SecureRandom.uuid
    self.metrics ||= {}
  end


  ocean_resource_model index: [:instance_id, :name, :ocean_env, :service, :subservice, :state],
                       search: false

  serialize :contents, Oj
  serialize :security_groups, Oj
  serialize :metrics, Oj

  attr_accessible :instance_id, :name, :description, :ocean_env, :service, :subservice, :contents,
                  :state, :instance_type, :launch_time, :availability_zone, :subnet_id,
                  :private_ip_address, :public_ip_address, :state_transition_reason,
                  :vpc_id, :security_groups, :state_reason_message, :metrics


  def uptime
    return false unless state == "running"
    return false if launch_time.blank?
    Time.now.utc.to_f - launch_time.to_f
  end


  def self.refresh_all
    Rails.logger.info "Refreshing Instance DB"
    response = $ec2.describe_instances  # (filters: [{name: "tag:ChefEnv", values: [OCEAN_ENV]}])
    iids = []
    response.reservations.each do |reservation|
      reservation.instances.each do |instance|
        refresh_from_struct(instance)
        iids << instance.instance_id
      end
    end
    Instance.where.not(instance_id: iids).destroy_all   # Must be destroy as we want to uncache removed instances.
    Instance.invalidate
    true
  rescue => e
    Rails.logger.fatal "Refreshing Instance DB crashed: #{e}"
    false
  end


  def self.refresh_from_struct(s)
    contents = JSON.parse(s.to_json) #s.to_hash
    instance_id = contents['instance_id']
    tags = contents['tags'].inject({}) { |memo, h| memo[h['key']] = h['value']; memo }
    name = tags['Name']
    ocean_env = tags['ChefEnv']
    service = tags['Service']
    subservice = tags['Subservice'] || ""
    state = contents['state']['name']
    instance_type = contents['instance_type']
    launch_time = contents['launch_time'] if contents['launch_time']
    availability_zone = contents['placement']['availability_zone']
    subnet_id = contents['subnet_id']
    private_ip_address = contents['private_ip_address']
    public_ip_address = contents['public_ip_address']
    state_transition_reason = contents['state_transition_reason']
    vpc_id = contents['vpc_id']
    state_reason_message = contents['state_reason'] && contents['state_reason']['message']
    security_groups = contents['security_groups']
    contents['metrics'] = metrics = state == 'running' ? compile_metrics(instance_id) : {}
    # Find the instance. Create or update as required.
    i = find_by_instance_id(instance_id)
    if !i
      create! instance_id: instance_id, name: name, description: "",
              ocean_env: ocean_env, service: service, subservice: subservice,
              contents: contents, state: state, instance_type: instance_type,
              launch_time: launch_time, availability_zone: availability_zone,
              subnet_id: subnet_id, private_ip_address: private_ip_address,
              public_ip_address: public_ip_address, state_transition_reason: state_transition_reason,
              vpc_id: vpc_id, state_reason_message: state_reason_message,
              security_groups: security_groups, metrics: metrics
    else
      if contents.to_json != i.contents.to_json
        begin
          i.update_attributes name: name,
              ocean_env: ocean_env, service: service, subservice: subservice,
              contents: contents, state: state, instance_type: instance_type,
              launch_time: launch_time, availability_zone: availability_zone,
              subnet_id: subnet_id, private_ip_address: private_ip_address,
              public_ip_address: public_ip_address, state_transition_reason: state_transition_reason,
              vpc_id: vpc_id, state_reason_message: state_reason_message,
              security_groups: security_groups, metrics: metrics
        rescue ActiveRecord::StaleObjectError
          retry
        end
      end
    end
  end


  def self.compile_metrics(instance_id)
    # Compile all metrics
    metrics = {}
    # Retrieve the CPUUtilization metrics
    resp = $cloudwatch.get_metric_statistics(namespace: "AWS/EC2",
                                             metric_name: "CPUUtilization",
                                             unit: "Percent",
                                             statistics: ["Average"],
                                             dimensions: [{name: "InstanceId", value: instance_id}],
                                             period: 60,
                                             start_time: 1.hour.ago.utc,
                                             end_time: Time.now.utc)
    dp = []
    resp.datapoints.each do |p|
      dp << [p.timestamp.utc.iso8601, p.average]
    end
    dp.sort!
    metrics[resp.label] = dp
    # Retrieve the NetworkIn metrics
    resp = $cloudwatch.get_metric_statistics(namespace: "AWS/EC2",
                                             metric_name: "NetworkIn",
                                             unit: "Bytes",
                                             statistics: ["Maximum"],
                                             dimensions: [{name: "InstanceId", value: instance_id}],
                                             period: 60,
                                             start_time: 1.hour.ago.utc,
                                             end_time: Time.now.utc)
    dp = []
    resp.datapoints.each do |p|
      dp << [p.timestamp.utc.iso8601, p.maximum]
    end
    dp.sort!
    metrics[resp.label] = dp
    # Retrieve the NetworkOut metrics
    resp = $cloudwatch.get_metric_statistics(namespace: "AWS/EC2",
                                             metric_name: "NetworkOut",
                                             unit: "Bytes",
                                             statistics: ["Maximum"],
                                             dimensions: [{name: "InstanceId", value: instance_id}],
                                             period: 60,
                                             start_time: 1.hour.ago.utc,
                                             end_time: Time.now.utc)
    dp = []
    resp.datapoints.each do |p|
      dp << [p.timestamp.utc.iso8601, p.maximum]
    end
    dp.sort!
    metrics[resp.label] = dp
    # return metrics
    metrics
  end


  def start(additional_info=nil)
    args = {instance_ids: [instance_id]}
    args[:additional_info] = additional_info unless additional_info.blank?
    $ec2.start_instances(args)
    set_pending_and_save
    Rails.logger.info "Starting #{name}"
    true
  rescue => e
    Rails.logger.fatal "Couldn't start #{name}: #{e}"
    false
  end


  def stop
    $ec2.stop_instances(instance_ids: [instance_id])
    set_pending_and_save
    Rails.logger.info "Stopping #{name}"
    true
  rescue => e
    Rails.logger.fatal "Couldn't stop #{name}: #{e}"
    false
  end


  def reboot
    $ec2.reboot_instances(instance_ids: [instance_id])
    set_pending_and_save
    Rails.logger.info "Rebooting #{name}"
    true
  rescue => e
    Rails.logger.fatal "Couldn't reboot #{name}: #{e}"
    false
  end


  def terminate
    $ec2.terminate_instances(instance_ids: [instance_id])
    set_pending_and_save
    Rails.logger.info "Terminating #{name}"
    true
  rescue => e
    Rails.logger.fatal "Couldn't terminate #{name}: #{e}"
    false
  end


  def set_pending_and_save
    begin
      self.state = 'pending'
      save!
    rescue ActiveRecord::StaleObjectError
      reload
      retry
    end
  end

end
