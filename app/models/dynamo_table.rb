class DynamoTable

  include ActiveModel::Model

  attr_accessor :name
  attr_accessor :table

  validates :name, presence: true
  validate :get_table

  def get_table
    # NB: Validation triggers fetching of the table object
    return unless name.present?
    resource = Aws::DynamoDB::Resource.new(client: $dynamo)
    t = resource.table(name)
    return if t.data_loaded?
    begin
      self.table = t.load
    rescue #Aws::DynamoDB::Errors::ResourceNotFoundException
      errors.add :name, "doesn't signify an existing DynamoDB table"
      self.table = nil
    end
  end

  def table_arn
    table.table_arn
  end

  def attribute_definitions
    table.attribute_definitions.collect &:to_h
  end

  def key_schema
    table.key_schema.collect &:to_h
  end

  def global_secondary_indexes
    ixs = table.global_secondary_indexes
    ixs && ixs.collect do |elem|
      elem = elem.to_h
      pt = elem[:provisioned_throughput]
      lidt = pt[:last_increase_date_time]
      pt[:last_increase_date_time] = lidt.utc.iso8601 if lidt.present? && !lidt.is_a?(String)
      lddt = pt[:last_decrease_date_time]
      pt[:last_decrease_date_time] = lddt.utc.iso8601 if lddt.present? && !lddt.is_a?(String)
      elem
    end
  end

  def local_secondary_indexes
    ixs = table.local_secondary_indexes
    ixs && ixs.collect(&:to_h)
  end

  def provisioned_throughput
    d = table.provisioned_throughput.to_h
    lidt = d[:last_increase_date_time]
    d[:last_increase_date_time] = lidt.utc.iso8601 if lidt.present? && !lidt.is_a?(String)
    lddt = d[:last_decrease_date_time]
    d[:last_decrease_date_time] = lddt.utc.iso8601 if lddt.present? && !lddt.is_a?(String)
    d
  end

  def table_status
    table.table_status
  end

  def item_count
    table.item_count
  end

  def table_size_bytes
    table.table_size_bytes
  end

  def stream_specification
    table.stream_specification.to_h
  end

  def latest_stream_arn
    table.latest_stream_arn
  end

  def latest_stream_label
    table.latest_stream_label
  end

  def created_at
    table.creation_date_time.utc   # NB: Time object, not a string
  end


  def destroy
    table.delete
  end


  def self.delete_test_tables
    obtain_all_dynamodb_tables.each do |table_name|
      delete_table(table_name) if purgeable?(table_name)
    end
  end

  def self.obtain_all_dynamodb_tables
  	# Add paging here - more than one request may be necessary to get items 100+ etc
    $dynamo.list_tables.table_names
  end

  def self.purgeable?(table_name)
  	!!(table_name =~ Regexp.new("^.+_(test|master)$"))
  end

  def self.delete_table(table_name)
    Rails.logger.info "Deleting DynamoDB table #{table_name}"
    begin
  	  $dynamo.delete_table(table_name: table_name)
    rescue Aws::DynamoDB::Errors::LimitExceededException
      Rails.logger.debug "AWS deletion failed due to deletion limit reached, waiting and retrying..."
      sleep 10
      retry
    end
  end


  def self.all
    DynamoTable.obtain_all_dynamodb_tables.collect do |name|
      t = DynamoTable.new name: name
      t.valid?
      t
    end
  end



  # The following two methods are for api_render
  def self.to_partial_path
    "dynamo_tables/_dynamo_table"
  end

  def self.table_name
    "dynamo_tables"
  end

end
