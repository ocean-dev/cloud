class LoadBalancer

	include ActiveModel::Model

    attr_accessor :load_balancer_name, :created_time, :availability_zones, :backend_server_descriptions,
                  :canonical_hosted_zone_name, :canonical_hosted_zone_name_id, :dns_name,
                  :source_security_group, :listener_descriptions, :subnets, :instances,
                  :vpc_id, :scheme, :policies, :health_check, :security_groups, :metrics


  def initialize(*)
    super
    self.metrics = get_metrics unless load_balancer_name.blank?
  end


	def self.table_name
	  "load_balancers"
	end


	def self.all
	  result = []
	  marker = nil
	  loop do
        batch = $elb.describe_load_balancers marker: marker
        batch.load_balancer_descriptions.each do |lb|
      	  result << LoadBalancer.new(lb.to_hash)
      	end
      	marker = batch.next_marker
      	break unless marker
      end
	  result
	end


	def self.find_by_load_balancer_name (name)
      batch = $elb.describe_load_balancers(load_balancer_names: [name])
      lb = batch.load_balancer_descriptions.first
      lb && LoadBalancer.new(lb.to_hash)
	end


	def created_at
	  created_time
	end


  def get_metrics
    metrics = {}
    %w(HealthyHostCount UnHealthyHostCount RequestCount Latency 
       SurgeQueueLength SpilloverCount HTTPCode_ELB_4XX HTTPCode_ELB_5XX 
       HTTPCode_Backend_2XX HTTPCode_Backend_3XX HTTPCode_Backend_4XX HTTPCode_Backend_5XX
       BackendConnectionErrors
    ).each do |metric_name|
      resp = $cloudwatch.get_metric_statistics(namespace: "AWS/ELB", 
                                               metric_name: metric_name, 
                                               statistics: ["Sum", "Maximum", "Minimum", "Average"], 
                                               dimensions: [{name: "LoadBalancerName", value: load_balancer_name}], 
                                               period: 60, 
                                               start_time: 1.hour.ago.utc, 
                                               end_time: Time.now.utc)
      dp = []
      resp.datapoints.each do |p|
        dp << [p.timestamp.utc.iso8601, p.maximum, p.minimum, p.average, p.sum]
      end
      dp.sort!
      metrics[resp.label] = dp
    end
    metrics
  end

end
